/*--------------------------------------------------------------------------
Example.c
****************************************
**  Copyright  (C)    2021-2022   **
**  Web:              http://rothd.cn   **
****************************************
--------------------------------------------------------------------------*/

#include "MC3172.h"
#include <math.h>
#include "common.h"
////////////////////////////////////////////////////////////

// 延时微秒nop
void delay_us_nop(u32 nus)
{
    // u32 nops = nus * CORE_CLOCK_MHZ_DIV4 / 3;
    // while (nops--)
    // {
    //     NOP();
    // }
    DELAY_US_NOP(nus);      // 根据nop延时，每次1us
}

// 延时毫秒
void delay_ms_nop(u32 n) //循环1000次就是1ms
{
    DELAY_US_NOP(n * 1000); // 1ms延时
}

// 延时秒
void delay_nop(u32 i) //循环1000 * 1000次就是1s
{
    DELAY_US_NOP(i * 1000 * 1000); // 1s延时
}

// 延时微秒
void delay_us(u32 nus)
{
    // u32 ticks;
    // u32 told, tnow, tcnt = 0;
    // ticks = nus * CORE_CLOCK_MHZ_DIV4; //目标延时节拍数=需要延时时间(us)*CORE_CLK(MHz)/4
    // GET_CORE_CNT(told);             //读取内核定时器数值
    // while (1)
    // {
    //     GET_CORE_CNT(tnow);
    //     if (tnow != told)
    //     {
    //         if (tnow < told)
    //             tcnt = 0xFFFFFFFF - told + tnow; // CORE_CNT递增32位，计算已延时节拍数
    //         else
    //             tcnt = tnow - told;
    //         if (tcnt >= ticks)
    //             break; //延时完成
    //     }
    // };
    DELAY_US_CNT(nus);      // 根据cnt延时，每次1us
}

// 延时毫秒
void delay_ms(u32 n) //循环1000次就是1ms
{
    DELAY_US_CNT(n * 1000);      // 延时1ms
}

// 延时秒
void delay(u32 i) //循环1000*1000次就是1s
{
    DELAY_US_CNT(i * 1000 * 1000);      // 延时1s
}
////////////////////////////////////////////////////////////
