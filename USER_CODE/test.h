#ifndef __GPIO_GPCOM_TEST_EXAM__
#define __GPIO_GPCOM_TEST_EXAM__

void LED0_GPIOA_PIN0_TEST(void);
void LED1_GPIOA_PIN1_TEST(void);
void LED2_GPIOA_PIN2_TEST(void);
void LED6_GPIOA_PIN6_TEST(void);
void GPCOM_SPI_WS2812B(u32 gpcom_sel);

#endif