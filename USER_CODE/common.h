/*--------------------------------------------------------------------------
Example.c
****************************************
**  Copyright  (C)    2021-2022   **
**  Web:              http://rothd.cn   **
****************************************
--------------------------------------------------------------------------*/

#ifndef _COMMON_
#define _COMMON_
#include "./MC3172.h"
#include "./thread_config.h"

////////////////////////////////////////////////////////////

#if ROTHD_COLCK_SOURCE_SEL==0
#define CORE_CLOCK_MHZ 48   // X3
#endif
#if ROTHD_COLCK_SOURCE_SEL==1
#define CORE_CLOCK_MHZ 200  // X5
#endif
#if ROTHD_COLCK_SOURCE_SEL==2
#define CORE_CLOCK_MHZ 200  // RC
#endif
#if ROTHD_COLCK_SOURCE_SEL==3
#define CORE_CLOCK_MHZ 8  // RC with slow speed
#endif

#define CORE_CLOCK_MHZ_DIV2 CORE_CLOCK_MHZ / 2
#define CORE_CLOCK_MHZ_DIV4 CORE_CLOCK_MHZ / 4
#define CORE_CLOCK_MHZ_DIV8 CORE_CLOCK_MHZ / 8

#define CORE_CLOCK_HZ CORE_CLOCK_MHZ * 1000000
#define CORE_CLOCK_HZ_DIV2 CORE_CLOCK_HZ / 2
#define CORE_CLOCK_HZ_DIV4 CORE_CLOCK_HZ / 4
#define CORE_CLOCK_HZ_DIV8 CORE_CLOCK_HZ / 8

#define DELAY_US_NOP(nus) ({                   \
    u32 nops = nus * CORE_CLOCK_MHZ_DIV4 / 3;\
    while (nops--)                           \
    {                                        \
        NOP();                               \
    }                                        \
})

#define DELAY_US_CNT(nus) ({                  \
    u32 ticks;                              \
    u32 told, tnow, tcnt = 0;               \
    ticks = nus * CORE_CLOCK_MHZ_DIV4;      \
    GET_CORE_CNT(told);                     \
    while (1)                               \
    {                                       \
        GET_CORE_CNT(tnow);                 \
        if (tnow != told)                   \
        {                                   \
            if (tnow < told)                \
                tcnt = 0xFFFFFFFF - told + tnow; \
            else                            \
                tcnt = tnow - told;         \
            if (tcnt >= ticks)              \
                break;                      \
        }                                   \
    }                                       \
})

// 延时微秒nop
void delay_us_nop(u32 nus);
// 延时毫秒nop
void delay_ms_nop(u32 nus); //循环1000次就是1ms
// 延时秒nop
void delay_nop(u32 nus); //循环1000*1000次就是1s

// 延时微秒
void delay_us(u32 nus);
// 延时毫秒
void delay_ms(u32 n); //循环1000次就是1ms
// 延时秒
void delay(u32 i); //循环1000*1000次就是1s

////////////////////////////////////////////////////////////
#endif