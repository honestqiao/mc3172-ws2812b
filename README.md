## 说明

本实例为MC3172开发板，使用SPI控制WS2812B灯珠/灯环的实例
对应分享文章为: [https://whycan.com/t_8663.html](https://whycan.com/t_8663.html)


## 硬件说明：

* MC3172：
  * 晶振：X5，使用200MHz
  * SPI：使用GPCOM1，PA5对应CLK(并无实际使用，逻辑分析仪调试用)，PA6对应MOSI用于WS2812B控制
* WS2812B：
  * 电源：外界5V电源
  * DIN：控制数据输入，连接到MC3172开发板的PA6



## 软件说明：

* common.h中，有关于晶振使用的定义
* test_spi.c中，有关于灯珠数量的配置

## 使用说明：

* 连接好上述硬件
* 编译并下载
* 观察灯珠/灯环状态

